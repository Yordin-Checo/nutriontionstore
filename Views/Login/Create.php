<!-- Registrarse-->

<div class="modal fade" id="Login" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary" id="exampleModalLabel"><i class="fas fa-file"></i> REGISTRAR USUARIO</h5>
                <button type="button" class="close bg-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class ="container">
                  <div class="col-md-12">
        <div class="form-horizontal">
<form action="index.php?L=Registrar" method="post">

<div class="form-row">
       <div class="form-group col-md-6">
            <label class="col-sm-2 control-label" for="txt_nombre">Nombre: </label>
                   <div class="col-sm-11">
                        <input type="text" name="txt_Nombre" class="form-control">        
                   </div>        
        </div>
        <div class="form-group col-md-6">
            <label class="col-sm-2 control-label" for="txt_nombre">Apellido: </label>
                   <div class="col-sm-11">
                        <input type="text" name="txt_Apellido" class="form-control">        
                   </div>        
        </div>
       
</div>

<div class="form-row">
     <div class="form-group col-md-6">
          <label class="col-sm-9 control-label" for="txt_nombre">Fecha de nacimiento: </label>
                 <div class="col-sm-11">
                      <input type="date" name="txt_Fecha" id="fecha" class="form-control">        
                 </div>        
     </div>
     <div class="form-group col-md-6">
          <label class="col-sm-2 control-label" for="txt_nombre">Dirección: </label>
                 <div class="col-sm-11">
                      <input type="text" name="txt_Direccion" class="form-control">        
                 </div>        
    </div>     
</div>


<div class="form-row">
     <div class="form-group col-md-6">
          <label class="col-sm-6 control-label" for="txt_nombre">Correo: </label>
                 <div class="col-sm-11">
                      <input type="email" name="txt_Email" class="form-control">        
                 </div>        
     </div>
     <div class="form-group col-md-6">
          <label class="col-sm-2 control-label" for="txt_nombre">Celular: </label>
                 <div class="col-sm-11">
                      <input  type="text" name="txt_Celular" class="form-control">        
                 </div>        
     </div>
      
 </div>

<div class="form-row">
     <div class="form-group col-md-6">
          <label class="col-sm-6 control-label" for="txt_nombre">User: </label>
                 <div class="col-sm-11">
                      <input type="text" name="txt_User" class="form-control">        
                 </div>        
     </div>
     <div class="form-group col-md-6">
          <label class="col-sm-2 control-label" for="txt_nombre">Password: </label>
                 <div class="col-sm-11">
                      <input  type="password" name="txt_Pass" class="form-control">        
                 </div>        
     </div>
      
 </div>
</div>
</div> 
</div> 
        <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar </button>
                <button type="submit" class="btn btn-primary">Registrar</button>
            </div>
        </div>

    </Form>
    </div>
</div>
          
     
<script type="text/javascript" src="Styles/fecha.js"></script>

<script>
   $(document).ready(function()
   {
      $("#Login").modal("show");
     
   });
</script>

    