<div class="container">
    <h1 style="text-align:center">Nosotros</h1>
    <br>
    <div class="row">
        <section>
            <div class="row col-md-12">
                <br>
                <article class="col-sm-8">
                    <p style="text-align:justify">
                    Somos  una empresa dedicada a la importación y comercializacion de susplemento alimenticios en República Dominicana, aportando valor nutricional
                    a las necesidades nutricionales y farmacólogicas de los pacientes en el país, ofreciendo la máxima calidad con cada unos de los productos que 
                    ofrecemos para mejorar el bienestar y la salud. de la sociedad. Suministramos una amplia gama de productos de calidad. Creemos en un modelo de bienestar
                    moderno que puede ser aplicado diariamente por todas las personas en el mundo. Nuestros
                    valores apuntan a difundir una cultura consciente basada en la importancia de la actividad física
                    y la atención a la salud. Queremos ofrecer las herramientas y productos para integrar nuestros
                    valores en la vida diaria de todos"
                    </p>
                </article>
                <article class="col-sm-4">
                    <div>
                        <img style="height:auto; width:100%;"  src="Img/saludable.jpg" alt="">
                    </div>
                </article>
            </div>
        </section>
        <br>
        <br>
        <br>
        <section>
            <div class="row col-md-12">
                <br>
                <article class="col-sm-2">
                    <div>
                        <img style="height:100%; width:100%;"  src="Img/maria.jpg" alt="">
                    </div>
                    <h6 style="text-align:center;">María Gómez</h6>
                    <p style="text-align:center;">Nutrióloga</p>
                </article>
                <article class="col-sm-10">
                    <p style="text-align:justify">
                        Con más de 10 años de experiencia ayudando a pacientes a resolver sus problemas de nurtrición, graduada en en la universidad Evángelica Nacional

                    </p>
                </article>
            </div>
        </section>
    </div>
    <br>
<hr>
<!-- Three columns of text below the carousel -->
<div class="row">
    <div class="col-lg-4">
      <img class="rounded-circle" src="Img/Mision.png" alt="Generic placeholder image" width="140" height="140">
   <hr>
      <p class="text-justify">Ser la empresa líder teniendo en la venta de suplementos alimenticios de la mano con solución a las necesidades de nuestros clientes cumpliendo siempre con la calidad, precio y servicio.</p>
   <p>
    </div><!-- /.col-lg-4 -->
    <div class="col-lg-4 justify-content-center">
    <img class="rounded-circle" src="Img/Valores.png" alt="Generic placeholder image" width="140" height="140">
   <hr>
   <p class="text-justify">Respeto, Disciplina, Compromiso, Honestidad. </P>
    </div><!-- /.col-lg-4 -->
    <div class="col-lg-4">
      <img class="rounded-circle" src="Img/Vision.png" alt="Generic placeholder image" width="140" height="140">
      <hr>
      <p  class="text-justify">Crecer en el mercado de suplementos alimenticios, sin olvidarnos de los altos estándares de calidad de nuestros productos y excelente servicio al cliente.</p>
    </div><!-- /.col-lg-4 -->
  </div><!-- /.row -->
  </div><!-- /.row -->

<hr>
<h5 class="text-dark text-primary text-center font-weight-bold">Manténte en contacto con nosotros</h5>
  
<br />
        <p class="text-center text-justify ">Teléfono: 809-000-0000 829-000-1111</p>

        <p class="text-center text-justify ">E-mail: NutritionStore@PRUEBA.COM</p>
    <br>
    <br>
</div>

