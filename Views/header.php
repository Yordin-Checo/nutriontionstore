<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="Iconos/favicon.ico">

    <title>Tienda Web</title>
    <!-- Bootstrap core CSS -->
    <link href="Styles/dist/css/bootstrap.min.css" rel="stylesheet">
    <link href="Styles/carousel.css" rel="stylesheet">
    <script type="text/javascript" src="Styles/fecha.js"></script>
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">
    <script src="//code.jquery.com/jquery-1.12.0.min.js"></script>
  </head>
  <body>
    <header>
<!--Fijar menu fixed-top-->
        <nav class="navbar navbar-expand-sm fixed-top navbar-toggleable-sm navbar-light bg-primary border-bottom box-shadow mb-3">
            <div class="container  " id="navbarHeader">
            <img  src="Img/Logo.png" alt="Logo" >
             <!--   <a class="navbar-brand text-white"  href="index.php"><i class="fas fa-globe"></i> Tienda Web</a>-->
          
                <button class="navbar-toggler bg-white" type="button" data-toggle="collapse" data-target=".navbar-collapse" aria-controls="navbarSupportedContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="navbar-collapse collapse d-sm-inline-flex flex-sm-row-reverse">
                    <partial name="_LoginPartial" />
                    <ul class="navbar-nav flex-grow-1">
                        <li class="nav-item">
                            <a class="nav-link text-white" href="index.php?L=Nosotros" ><i class="fas fa-users"></i> Nosotros</a>
                        </li> 
                        <li class="nav-item">
                            <a class="nav-link text-white" href="index.php?P=Tienda " ><i class="fas fa-cart-arrow-down"></i> Tienda</a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link text-white" href="index.php?C=index" ><i class="fas fa-cart-arrow-down"></i> Carrito</a>
                        </li>
                        <?php
                        if(isset($_SESSION['User']) and  $_SESSION['estado'] == true){
                        if($_SESSION['Rol'] == "1"  ) { ?>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="index.php?P=index"><i class="fas fa-cubes"></i> Producto</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link text-white" href="index.php?Z=index"><i class="fas fa-cubes"></i> Categoría</a>
                        </li>
                        
                        <li class="nav-item">
                            <a class="nav-link text-white" href="index.php?T=index"><i class="fas fa-restroom"></i> Clientes</a>
                        </li>
                        <?php } ?>
                        <li class="nav-item">
                                <a class="nav-link text-white" >User <?php echo $_SESSION['Nombre']?>!</a>
                                </li>
                                <label class="text-white"></label>
                                <?php } ?>
                                <!-- User -->
                              
                                    
                                
                                <?php if(isset($_SESSION['User']) and  $_SESSION['estado'] == true) { ?>
                                    
                        
                                    <!-- Admin -->
                                      <div class="dropdown show">
                                    <a class="nav-link text-white dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-arrow-circle-down"></i> Logout
                                    </a>
    
                                    <div class="dropdown-menu bg-info" aria-labelledby="dropdownMenuLink">
                                      <?php if($_SESSION['Rol'] == "1"  ) { ?>
                                       <!-- Login -->
                                        <a class="nav-link text-white" href="index.php?L=Rol"  > <i class="fas fa-users-cog"></i> Adminstrar User</a>
                                        <a class="nav-link text-white" href="index.php?L=Create"  > <i class="fas fa-address-card"></i> Registrarse</a>
                                       <!-- <a class="nav-link text-white" href="index.php?L=Rol_A"  > <i class="fas fa-user-plus"></i> Registrar Roles</a>-->
                                      <?php } ?>
                                      <!--Salir-->
                                        <a class="nav-link text-white" href="index.php?L=closet" > <i class="fas fa-sign-in-alt"></i> Logout</a>
                                    </div>
    
                                        <?php } else { ?>
                                    <div class="dropdown show">
                                    <a class="nav-link text-white dropdown-toggle" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="fas fa-arrow-circle-down"></i> Login
                                    </a>
                                        <?php } ?>
                                    <div class="dropdown-menu bg-info" aria-labelledby="dropdownMenuLink">
                                                          <!-- Inicio -->
                                        <a class="nav-link text-white" href="index.php?L=index" > <i class="fas fa-sign-in-alt"></i> Iniciar sesión</a>
                                        <a class="nav-link text-white" href="index.php?L=Create"  > <i class="fas fa-address-card"></i> Registrarse</a>
                                    </div>
                                   
                                   
                                </div>
                         
                       
                    </ul>
                </div>
            </div>
        </nav>   
      </header>
      <br>
      <br> 
      <br>