<div class="container" styles="margin-top:80px">
    <div class="jumbotron">
       <h2 align="center">REGISTRO DE CLIENTE</h2>
    </div>
<hr>

    <form class="form-inline  " action="index.php?C=Buscar" method="Post" >
    <div class="form-group col-md-12">
        <input type="text" class="form-control" name="txt_buscar" placeholder="CLIENTE">
        &nbsp;
        <span class="input-group-btn" style="width:25%;">
            <input class="btn btn-dark text-white form-control" type="submit" value="Buscar" />
        </span>
        
    </div>
</form>
<hr>

<div class="table-responsive">
    <table class="table table-bordered table-hover">
        <thead class="thead-dark">
                <tr>
                    <th scope="row">Código</th>
                    <th scope="row">Nombre</th>
                    <th scope="row">Dirección</th>                    
                    <th scope="row">Celular</th>
                    <th scope="row">Email</th>
                    <th scope="row">Acción</th> 
                </tr>
            </thead>
            <tbody>
          
                <?php foreach($query as $data):?>
                <tr>
                    <td><?php echo $data['IdCliente']?></td>
                    <td><?php echo $data['Nombre']?> <?php echo $data['Apellido']?></td>             
                    <td><?php echo $data['Direccion']?></td>                   
                    <td><?php echo $data['Celular']?></td>
                    <td><?php echo $data['Correo']?></td>                   
                    <td>
                    <a class="text-danger ml-1" data-toggle='modal' data-target='#EliminarC' onClick='selectDelete(this)'><i class="fa fa-trash-alt fa-lg "></i></a>
                    </td>
                </tr>
               <?php endforeach;?>
            
            </tbody>
        </table>
    </div>
</div>

<!-- Eliminar-->
<div class="modal fade" id="EliminarC" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary" id="exampleModalLabel"><i class="fas fa-restroom"></i> ELIMINAR CLIENTE</h5>
                <button type="button" class="close bg-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class ="container">
                  <div class="col-md-12">
        <div class="form-horizontal">


<form action="index.php?C=Delete" method="post">

<h3 class="text-danger" align="center">¿Seguro que desea eliminar el cliente?</h3>
<hr>
    <div class="form-row">
    <div class="form-group col-md-6">
             <label class="col-sm-6 control-label" for="txt_Codigo">Código Cliente: </label>
                <div class="col-sm-10">
                    <input type="text" name="txt_Codigo" id="Id" class="form-control" readonly>        
                </div>        
        </div>
        <div class="form-group col-md-6">
             <label class="col-sm-2 control-label" for="txt_nombre">Nombre: </label>
                <div class="col-sm-10">
                    <input type="text" name="txt_nombre" id="Nom" class="form-control" readonly>        
                </div>        
        </div>
       

    </div>   
   </div>
</div>
</div> 
</div> 
        <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar </button>
                <button type="submit" class="btn btn-danger">Eliminar</button>
            </div>
        </div>

    </Form>
    </div>
</div>


<script>
//Función para pasar los parametros a nuestra ventana modal
function selectCliente(boton) {

var celdas = $(boton).closest('tr').find('td');

var Codigo_Cliente   = celdas.eq(0).text();
var Nombre           = celdas.eq(1).text();
var Apellido         = celdas.eq(2).text();
var Direccion        = celdas.eq(3).text();
var Celular          = celdas.eq(4).text();
var Email            = celdas.eq(5).text();
var Estado           = celdas.eq(6).text();

$('#Codigo_Cliente').val(Codigo_Cliente);
$('#Nombre').val(Nombre);
$('#Apellido').val(Apellido);
$('#Direccion').val(Direccion);
$('#Celular').val(Celular);
$('#Email').val(Email);
$('#Estado').val(Estado);

}
function selectDelete(boton)
{
var celdas = $(boton).closest('tr').find('td');

var Codigo_Cliente = celdas.eq(0).text();
var Nombre         = celdas.eq(1).text();
var Apellido       = celdas.eq(2).text();

$('#Id').val(Codigo_Cliente);
$('#Nom').val(Nombre);
}
</script>
