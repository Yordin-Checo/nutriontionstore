<div class="container" styles="margin-top:80px">
    <div class="jumbotron">
       <h2 align="center">CARRITO DE COMPRA</h2>
    </div>
   
    <hr />
<?php if(isset($_SESSION['User']) and  $_SESSION['estado'] == true){  $User = $_SESSION['ID'] ; }?>
<!-- Inicio-->
<form method="POST" action="result.php">
<div class="table-responsive">
    <table class="table table-bordered table-hover">
    <thead class="thead-dark">
                <tr>
                    
                    <th scope="row">Producto</th>
                    <th scope="row">Precio</th>              
                    <th scope="row">Cantidad</th>    
                    <th scope="row">SubTotal</th> 
                    <th scope="row">Accion</th> 
                </tr>
            </thead>
            <tbody>
                <?php $total=0; foreach($datos as $data):?>
                <tr>
                    <?php $total = $total+$data['SubTotal']; ?>
                    <td hidden ><?php echo $data['IdCarrito']?></td>
                    <td><?php echo $data['Nombre']?></td>
                    <td>RD$<?php echo number_format($data['Precio'],2)?></td>                 
                    <td><?php echo  $data['Cantidad']?></td>
                    <td>RD$<?php echo number_format($data['SubTotal'],2)?></td> 
                    <td>
                    <a  class='text-success ml-1'  data-toggle='modal' data-target='#EditarP' onClick='selectProducto(this)'><i class="fa fa-marker fa-lg "></i></a>      
                    <a class="text-danger ml-1" data-toggle='modal' data-target='#EliminarP' onClick='selectDelete(this)'><i class="fa fa-trash-alt fa-lg "></i></a>
                    </td>
                </tr>
               <?php endforeach;?>
            </tbody>
        </table>
    </div>
 
    <div class="form-row">
    <div class="form-group  col-md-5 "><?php if(isset($_SESSION['User']) and  $_SESSION['estado'] == true){ $monto=  number_format($total,2); echo"
    <h4 class='text-left'>Monto Total: RD$ $monto </h4>"; }?>
    </div>

    <div class="form-group col-md-7 d-flex justify-content-end">
    <?php
    if(isset($_SESSION['User']) and  $_SESSION['estado'] == true){
      echo "<button type='button' class='btn btn-dark onClick='Producto()'  ' data-toggle='modal' data-target='#exampleModal' data-whatever=''><i class='fas fa-money-check-alt'></i>  Comprar</button>
   "; }
    ?>
    </div>
    </div>
</div>
</Form>




<!-- Registrar-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary" id="exampleModalLabel"><i class="fas fa-money-check-alt"></i> REALIZAR PAGO</h5>
                <button type="button" class="close bg-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class ="container">
                  <div class="col-md-12">
        <div class="form-horizontal">
        
<form action="index.php?C=Pagar" method="post" >
<input type="hidden" name="txt_Codigo" value="<?php echo $total?>">
    <div class="form-row">
        <div class="form-group col-md-12">
             <label class="col-sm-12 control-label" for="txt_nombre">Número de Tarjeta: </label>
                <div class="col-sm-12">
                    <input type="text" name="txt_numero" class="form-control" required>        
                </div>        
        </div>
  </div>
    
    </div>
   
    <div class="form-row">
        <div class="form-group col-md-6">
             <label class="col-sm-12 control-label" for="txt_Nombre">Fecha de Expiración: </label>
                <div class="col-sm-12">
                    <input type="number" placeholder="Año"   name="txt_year" class="form-control" required>        
                </div>  
                      
        </div>
        <div class="form-group col-md-6">
        <label class="col-sm-12 control-label" for="txt_Nombre">Mes: </label>
             <div class="col-sm-12">
                  <input type="number" placeholder="Mes" required  name="txt_mes" class="form-control">         
             </div> 
        </div>  
         
    </div>

    <div class="form-row">
         <div class="form-group col-md-12">
              <label class="col-sm-12 control-label" for="txt_apellido">Código de Seguridad: </label>
                     <div class="col-sm-12">
                          <input type="number"   required name="txt_CodigoS" class="form-control">        
                     </div>        
         </div>
    </div>
   
</div>
</div> 
</div> 
</hr>
<H3 align="center">MONTO A PAGAR: RD$<?php echo number_format($total,2)  ?></H3>

        <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar </button>
                <button type="submit" class="btn btn-primary">Pagar</button>
            </div>
        </div>

    </Form>
    </div>
</div>


<!-- Editar-->
<div class="modal fade" id="EditarP" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary" id="exampleModalLabel"><i class="fas fa-cubes"></i> EDITAR PRODUCTO</h5>
                <button type="button" class="close bg-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class ="container">
                  <div class="col-md-12">
        <div class="form-horizontal">
        
<form action="index.php?C=Editar" method="post">
<input type="hidden" name="txt_Codigo" id="Codigo_Producto">
<div class="form-row">
<div class="form-group col-md-6">
          <label class="col-sm-2 control-label" for="txt_nombre">Nombre: </label>
                <div class="col-sm-11">
                     <input type="text" name="text_nombreP" required id="Nombre" class="form-control" readonly>        
                      
                </div>        
     </div>        
          <div class="form-group col-md-6">
               <label class="col-sm-2 control-label" for="txt_Nombre">Cantidad: </label>
                      <div class="col-sm-11">
                           <input type="number" name="txt_cantidad" id="Cantidad"  class="form-control" required>        
                      </div>        
          </div>      
         
  </div>
    </div>
</div>
</div>
</div>
 
        <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar </button>
                <button type="submit" class="btn btn-info">Editar</button>
            </div>
        </div>

    </Form>
    </div>
</div>

</div>
 

<!-- Eliminar-->
<div class="modal fade" id="EliminarP" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary" id="exampleModalLabel"><i class="fas fa-restroom"></i> ELIMINAR PRODUCTO</h5>
                <button type="button" class="close bg-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class ="container">
                  <div class="col-md-12">
        <div class="form-horizontal">


<form action="index.php?C=Delete" method="post">
<input type="hidden" name="txt_Codigo" id="Id">  
<h3 class="text-danger" align="center">¿Seguro que desea eliminar el producto?</h3>
<hr>
    <div class="form-row">
    <div class="form-group col-md-6">
         <label class="col-sm-2 control-label" for="txt_nombre">Nombre: </label>
                <div class="col-sm-10">
                    <input type="text" name="txt_nombre" id="Nom" class="form-control" readonly>        
                </div>       
                   
    </div>
           <div class="form-group col-md-6">
                <label class="col-sm-6 control-label" for="txt_Codigo">Cantidad: </label>
                       <div class="col-sm-10">
                            <input type="number" name="txt_cantidad" id="Canti"  class="form-control" readonly>          
                       </div>  
           </div>
       

    </div>   
   </div>
</div>
</div> 
</div> 
        <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar </button>
                <button type="submit" class="btn btn-danger">Eliminar</button>
            </div>
        </div>

    </Form>
    </div>
</div>

<script>
//Función para pasar los parametros a nuestra ventana modal
function selectProducto(boton) {

var celdas = $(boton).closest('tr').find('td');

var Codigo_Producto       = celdas.eq(0).text();
var Nombre                = celdas.eq(1).text();
var Cantidad              = celdas.eq(3).text();

$('#Codigo_Producto').val(Codigo_Producto);
$('#Nombre').val(Nombre);
$('#Cantidad').val(Cantidad); 
}

function Producto() {
//var variable =  document.getElementsByTagName("td");
var total = '<?php echo $total; ?>';
$('#Monto').val(total);
}

function selectDelete(boton)
{
var celdas = $(boton).closest('tr').find('td');

var Codigo_Producto     = celdas.eq(0).text();
var Descripcion         = celdas.eq(1).text();
var Cantidad            = celdas.eq(3).text();

$('#Id').val(Codigo_Producto);
$('#Nom').val(Descripcion);
$('#Canti').val(Cantidad); 
}
</script>
