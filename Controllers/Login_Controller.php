<?php
 

//Incluir archivos php del modelos para poder usarlos
require_once 'Models/Login_Model.php';
require_once 'Models/Producto_Model.php';


//Incluir archivos php model
Class Login_controller
{
//Variables
  private $model_Login;
  

  //Construtor para inicializar las varibles
  function __construct(){
    $this-> model_Login = new Login_Model();
    $this-> modelProducto = new Producto_Model;
  }

  
  function Nosotros(){
    require_once 'Views/header.php';
    require_once 'views/nosotros.view.php';
    require_once 'Views/footer.php';

  }

  function index()
  {
     require_once 'Views/header.php';
     require_once 'Views/Login/index.php';
     require_once 'Views/footer.php';
  }

  function Alerta($Mensaje1)
  {
      $Mensaje=$Mensaje1 ;
      require_once  'Views/header.php';
      require_once  'Views/Alerta.php';
      require_once  'Views/Login/index.php';
      require_once  'Views/footer.php';
     
  }

  function AlertaT($Mensaje1)
  {
    $datos = $this-> modelProducto-> getProducto();
    $categoria = $this-> modelProducto-> getCategoria();
      $Mensaje=$Mensaje1 ;
      require_once  'Views/header.php';
      require_once  'Views/Alerta.php';
      require_once  'Views/Tienda/index.php';
      require_once  'Views/footer.php';
     
  }

  function AlertaD($Mensaje1)
  {
      $Mensaje=$Mensaje1 ;

      $query = $this-> model_Login-> Roles();
      $Rol   = $this-> model_Login-> Rol();
      require_once 'Views/header.php';
      require_once  'Views/Alerta.php';
      require_once 'Views/Login/Roles.php';
      require_once 'Views/footer.php';
    
     
  }
  Function Create()
  {
    require_once  'Views/header.php';
    require_once  'Views/index.php';
    require_once  'Views/footer.php';
    require_once  'Views/Login/Create.php';     
  }
  
  function Rol()
  {
    $query = $this-> model_Login-> Roles();
    $Rol   = $this-> model_Login-> Rol();
  
    require_once 'Views/header.php';
    require_once 'Views/Login/Roles.php';
    require_once 'Views/footer.php';
  }
  function Edit()
  {
   
    $data  ['Rol']      = $_REQUEST['sel_rol']; 
    $data  ['Codigo']   = $_REQUEST['txt_Codigo'];  
    $this-> model_Login-> Edit($data);
    $Mensaje = 'ROL EDITADO CORRECTAMENTE!';
    $this -> AlertaD($Mensaje);
  }
  Function Rol_A()
  {
    $query = $this-> model_Login-> Roles();
    require_once 'Views/header.php';
    require_once 'Views/Login/Roles.php';
    require_once 'Views/footer.php';
  }

  function Registrar()
  { 
    $data  ['Nombre']      = $_REQUEST['txt_Nombre'];
    $data  ['Apellido']    = $_REQUEST['txt_Apellido'];
    $data  ['Fecha']       = $_REQUEST['txt_Fecha'];
    $data  ['Direccion']   = $_REQUEST['txt_Direccion'];
    $data  ['Correo']      = $_REQUEST['txt_Email'];
    $data  ['Celular']     = $_REQUEST['txt_Celular'];
    $data  ['Password']    = $_REQUEST['txt_Pass'];
    $data  ['User']        = $_REQUEST['txt_User'];
    $data  ['Codigo_Rol']  = 2;
    $data  ['Estado']      = "A";
    $query  = $this-> model_Login-> Registrar($data);
    

    if($query != false)
    {
      $Mensaje = 'USUARIO REGISTRADO CORRECTAMENTE!';
      $this -> Alerta($Mensaje);
    }
    else
    {
    
    $Mensaje ='EL USER '. $data  ['Email'] .' NO ESTA DISPONIBLE';   
    $this -> AlertaT($Mensaje);
    }
    

  }

  function Login()
  {
    $Dato ['User'] = $_POST['User'];
    $Dato ['Pass'] = $_POST['Pass'];
    $query = $this-> model_Login-> get($Dato);

if($query != false)
{

  foreach($query as $data):

  if($data['User'] == $Dato ['User']) 
 {   
   $_SESSION['User']       = $data['User']; 
   $_SESSION['Nombre']     = $data['Nombre']; 
   $_SESSION['ID']         = $data['IdCliente'];
   $_SESSION['Rol']        = $data['IdRol']; 
   $_SESSION['estado']     = true; 
   $datos = $this-> modelProducto-> getProducto();
   $categoria = $this-> modelProducto-> getCategoria();
   $Mensaje = '¡Bienvenido '. $data  ['Nombre'] .'!';
   require_once 'Views/header.php';
   require_once 'Views/Alerta.php';
   require_once  'Views/Tienda/index.php';
   require_once 'Views/footer.php';
 }  

  endforeach;
  }
  else
  {
      $Mensaje = "¡USUARIO INCORRRECTO INTENTE DE NUEVO!";
      require_once 'Views/header.php';
      require_once 'Views/Alerta.php';
      require_once 'Views/Login/index.php';
      require_once 'Views/footer.php';

  }
  
}

function closet()
{

  $_SESSION['estado']     = false; 
  $Mensaje ='GRACIAS '. $_SESSION['Nombre'] .' POR PREFERIRNO';   
  session_destroy();
  $this -> AlertaT($Mensaje);

}


function Buscar()
{
  $data['Buscar']= $_REQUEST['txt_buscar'];
  $query =  $this-> model_Login -> Bus($data);

   if($query != false){
    require_once 'Views/header.php';
    require_once 'Views/Login/Rol.php';
    require_once 'Views/footer.php';
    }

  else
  {
    $Mensaje = 'USUARIO NO EXISTE!';
    $this -> AlertaD($Mensaje);

  }
}

}
?>