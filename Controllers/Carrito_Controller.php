<?php
require_once 'Models/Carrito_Model.php';


class Carrito_Controller{
  
    //Variables
    private $modelCarrito;
   
    function __construct(){
        $this-> modelCarrito = new Carrito_Model;
    }

    function index(){
      if(isset($_SESSION['User']) and  $_SESSION['estado'] == true){
        $Buscar  =  $_SESSION['ID'];}
        else {
          $Buscar=-0;
        }
        //Ejecutamos el metodo get y enviamos los datos 
        $datos = $this-> modelCarrito-> getCarrito($Buscar);
        require_once 'Views/header.php';
        require_once 'Views/Carrito/index.php';
        require_once 'Views/footer.php';
        }

    function Card(){
        require_once 'Views/header.php';
        //index
        require_once 'Views/Carrito/cart_add.php';
        //Footer
        require_once 'Views/footer.php';
    }

    function Editar()
    {
      $data['Codigo_Carrito']    = $_REQUEST['txt_Codigo'];
      $data['Cantidad']          = $_REQUEST['txt_cantidad'];
      $this-> modelCarrito-> Editar($data);
      $Mensaje = 'PRODUCTO EDITADO CORRECTAMENTE!';
      $this -> Alerta($Mensaje);
    }
 
    function Comprar(){
      $ID = $_REQUEST['IdCliente'];
      $query = $this-> modelCarrito-> getCarrito($ID);
      $Mensaje = 'Comprar!';
      $this -> AlertaC($Mensaje);
    }

    function Pagar(){
      $Total = $_REQUEST['txt_Codigo'];
      $Pago['Total']   =  $_REQUEST['txt_Codigo'];
      $Pago['Numero']  =  $_REQUEST['txt_numero'];
      $Pago['Year']    =  $_REQUEST['txt_year'];
      $Pago['Mes']     =  $_REQUEST['txt_mes'];
      $Pago['Codigo']  =  $_REQUEST['txt_CodigoS'];

      $Tarjeta = $this-> modelCarrito -> Tarjeta($Pago);

      if($Tarjeta != false){
        $Buscar  =  $_SESSION['ID'];
        $query = $this-> modelCarrito-> getCarrito($Buscar);
        
        $Compra = $this-> modelCarrito-> AddCompra($query,$Buscar,$Total);
        $Mensaje = 'PAGO REALIZADO CORRECTAMENTE!';
        $this -> Alerta($Mensaje);
      }else{
        $Mensaje = 'DATOS INVALIDO O BALANCE INSUFICIENTE PARA REALIZAR ESTE PAGO!';
        $this -> Alerta($Mensaje);
      }

      
    }

    
  function Delete()
  {
  
    $data = $_REQUEST['txt_Codigo'];
    $this-> modelCarrito -> Delete($data);
    $Mensaje = 'PRODUCTO ELIMINADO CORRECTAMENTE!';
    $this -> Alerta($Mensaje);
    
  }
  
    function Alerta($Mensaje1)
    {
        $Buscar  =  $_SESSION['ID'];
        $datos = $this-> modelCarrito-> getCarrito($Buscar); 
        $Mensaje=$Mensaje1;
        require_once 'Views/header.php';
        require_once 'Views/Alerta.php';
        require_once 'Views/Carrito/index.php';
        require_once 'Views/footer.php';
    }
    function AlertaC($Mensaje1)
    {
        $datos = [];
        $Mensaje=$Mensaje1;
        require_once 'Views/header.php';
        require_once 'Views/Alerta.php';
        require_once 'Views/Carrito/index.php';
        require_once 'Views/footer.php';
    }
  

    }
?>