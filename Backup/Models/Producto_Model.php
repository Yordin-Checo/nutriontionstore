<?php

Class Producto_Model{

    //Variables
    Private $DB;
    Private $Producto;
    
    //Constructor
    function __construct(){
        //Pasamo la conexión a la variable
        $this-> DB = conexion::getConnection();
        $this-> Producto = array();
    }

    //Método buscar
    function getProducto(){
        //Consulta
        $query = $this-> DB -> query("Select P.*, C.Descripcion as Categoria from producto P inner join categoria C on P.IdCategoria=C.Idcategoria  Where P.Estado ='A'");
        //Recorremos los datos con un While y lo pasamo a una variable
        while ($datos = $query-> fetch_assoc()) {
            $this-> Producto[] = $datos;
        }
        
        return $this -> Producto;

    }

    //Metodo categoria
    Function getCategoria(){
        $query = $this-> DB -> query("Select * from categoria");
        
        while ($datos = $query-> fetch_assoc()) {
            $this-> Categoria[] = $datos;
        }

        return $this -> Categoria;
    }

    //Método Agregar
    function addProducto($data){
      
        $query = "Insert into Producto(Nombre, Cantidad, Descripcion, Costo,Precio, Estado, IdCategoria) Values".
        "('".$data['Nombre']."','".$data['Cantidad']."' ,'".$data['Descripcion']."','".$data['Costo']."', '".$data['Precio']."','".$data['Estado']."','".$data['Categoria']."')";
        mysqli_query($this -> DB, $query) or die('error\n'.mysqli_error($this -> DB));
    }

    //Método Editar 
    function editProducto($data){
        $query= " UPDATE producto SET Nombre ='".$data['Nombre']."',Cantidad ='".$data['Cantidad']."', Descripcion ='".$data['Descripcion']."',
        Costo='".$data['Costo']."',Precio='".$data['Precio']."',Estado='".$data['Estado']."',IdCategoria='".$data['Categoria']."' WHERE  IdProducto = '".$data['IdProducto']."'";
         mysqli_query($this -> DB, $query) or die('error\n'.mysqli_error($this -> DB));
    }
     
    //Método Inahabilitar 
    function delProducto($data){
        $query= " UPDATE producto SET Estado= 'I' WHERE  IdProducto = '".$data['IdProducto']."'"; 
        mysqli_query($this -> DB, $query) or die('error\n'.mysqli_error($this -> DB));
    } 

    //Método Buscar
    function busProducto($data){
        $name = $data['Buscar'];
        $query = "Select P.*, C.Descripcion as Categoria from producto P inner join categoria C on P.IdCategoria=C.Idcategoria  where P.nombre like '%$name%' and P.estado='A'";
        $datos = $this-> DB -> query($query);

        while ($fila = $datos -> fetch_assoc()) {
            $this-> data[]= $fila;
        }
        if(isset($this-> data)){
            return $this-> data;
        }
        else {
            return $this-> P= false;
        }

    }

}



?>