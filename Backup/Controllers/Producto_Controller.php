<?php
//Requerimo el modelo Producto para usarlo
require_once 'Models/Producto_Model.php';


class Producto_Controller{

    //Variables
    private $modelProducto;

    function __construct(){
        $this-> modelProducto = new Producto_Model;
    }

    function index(){
        //Ejecutamos el metodo get y enviamos los datos 
        $datos = $this-> modelProducto-> getProducto();
        $categoria = $this-> modelProducto-> getCategoria();
        //Header
        require_once 'Views/header.php';
        //index
        require_once 'Views/Producto/index.php';
        //Footer
        require_once 'Views/footer.php';
        }
        function Alerta($Mensaje1)
        {
            $datos = $this-> modelProducto-> getProducto();
            $categoria = $this-> modelProducto-> getCategoria();
            $Mensaje=$Mensaje1 ;
            require_once 'Views/header.php';
            require_once 'Views/Alerta.php';
            require_once 'Views/Producto/index.php';
            require_once 'Views/footer.php';
        }

        function AlertaT($Mensaje1)
        {
            $datos = $this-> modelProducto-> getProducto();
            $Mensaje=$Mensaje1 ;
            require_once 'Views/header.php';
            require_once 'Views/Alerta.php';
            require_once 'Views/Tienda/index.php';
            require_once 'Views/footer.php';
        }

        function Tienda(){
          //Ejecutamos el metodo get y enviamos los datos 
        $datos = $this-> modelProducto-> getProducto();
        //Header
        require_once 'Views/header.php';
        //index
        require_once 'Views/Tienda/index.php';
        //Footer
        require_once 'Views/footer.php';
        }
        function BuscarTienda()
        {
          $data['Buscar']= $_REQUEST['txt_buscar'];
          $datos =  $this-> modelProducto -> busProducto($data);
      
          if($datos != false){
            require_once 'Views/header.php';
            require_once 'Views/Tienda/index.php';
            require_once 'Views/footer.php';
          }
      
          
          else
          {
            $Mensaje = 'PRODUCTO NO EXISTE!';
            $this -> AlertaT($Mensaje);
        
          }
        }

        function Create()
        {
            
          //Lo agregamos todo en un arreglo
          $data['Nombre']            = $_REQUEST['txt_nombreP'];
          $data['Cantidad']          = $_REQUEST['txt_cantidad'];
          $data['Descripcion']       = $_REQUEST['txt_Descri'];
          $data['Costo']             = $_REQUEST['txt_costo'];
          $data['Precio']            = $_REQUEST['txt_precio'];
          $data['Estado']            = $_REQUEST['txt_estado'];
          $data['Categoria']         = $_REQUEST['sel_categoria'];
          $this-> modelProducto -> addProducto($data);
          $Mensaje = 'PRODUCTO REGISTRADO CORRECTAMENTE!';
          $this -> Alerta($Mensaje);
      
        }

        function Editar(){
          $data['IdProducto']        = $_REQUEST['txt_Codigo'];
          $data['Nombre']            = $_REQUEST['text_nombreP'];
          $data['Cantidad']          = $_REQUEST['txt_cantidad'];
          $data['Descripcion']       = $_REQUEST['txt_Descri'];
          $data['Costo']             = $_REQUEST['txt_costo'];
          $data['Precio']            = $_REQUEST['txt_precio'];
          $data['Estado']            = $_REQUEST['txt_estado'];
          $data['Categoria']         = $_REQUEST['sel_categoria'];

          $this-> modelProducto -> editProducto($data);
          $Mensaje = 'PRODUCTO EDITADO CORRECTAMENTE!';
          $this -> Alerta($Mensaje);
        }

        
       function Delete()
      {
  
       $data['IdProducto']        = $_REQUEST['txt_Codigo'];
       $this-> modelProducto -> delProducto($data);
       $Mensaje = 'PRODUCTO ELIMINADO CORRECTAMENTE!';
       $this -> Alerta($Mensaje);
    
      }

      function Buscar()
  {
    $data['Buscar']= $_REQUEST['txt_buscar'];
    $datos =  $this-> modelProducto -> busProducto($data);

    if($datos != false){
      require_once 'Views/header.php';
      require_once 'Views/Producto/index.php';
      require_once 'Views/footer.php';
    }

    
    else
    {
      $Mensaje = 'PRODUCTO NO EXISTE!';
      $this -> Alerta($Mensaje);
  
    }
   
  }

    }
?>