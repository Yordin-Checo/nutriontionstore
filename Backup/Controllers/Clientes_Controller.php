<?php
//Incluir archivos php del modelos para poder usarlos
require_once 'Models/Clientes_Model.php';

//Incluir archivos php model
Class Clientes_controller
{
  //Variables
  private $model_Cliente;
  
  //Construtor para inicializar las varibles
  function __construct(){
    $this-> model_Cliente = new Cliente_Model();
  }

  function index()
  {
    
    $query = $this-> model_Cliente-> getCliente();
      //Retunr vista 
      //Header
      require_once 'Views/header.php';
      //Index
      require_once 'Views/Cliente/index.php';
      //Footer
      require_once 'Views/footer.php';
  }

  function Alerta($Mensaje1)
  {
     $query = $this-> model_Cliente-> getCliente(); 
      $Mensaje=$Mensaje1 ;
      require_once 'Views/header.php';
      require_once 'Views/Alerta.php';
      require_once 'Views/Cliente/index.php';
      require_once 'Views/footer.php';
  }
  
  function Create()
  {
    //Lo agregamos todo en un arreglo
    $data['Nombre']       = $_REQUEST['txt_nombre'];
    $data['Apellido']     = $_REQUEST['txt_apellido'];
    $data['Celular']      = $_REQUEST['txt_telefono'];
    $data['Email']        = $_REQUEST['txt_email'];
    $data['Direccion']    = $_REQUEST['txt_direccion'];
    $data['Estado']       = $_REQUEST['txt_estado'];
    $this-> model_Cliente -> addCliente($data);
    $Mensaje = 'CLIENTE REGISTRADO CORRECTAMENTE!';
    $this -> Alerta($Mensaje);

  }

  
  function Editar()
  {
    //Lo agregamos todo en un arreglo
    $data['Codigo_Cliente']     = $_REQUEST['txt_Codigo'];
    $data['Nombre']             = $_REQUEST['txt_nombre'];
    $data['Apellido']           = $_REQUEST['txt_apellido'];
    $data['Celular']            = $_REQUEST['txt_telefono'];
    $data['Email']              = $_REQUEST['txt_email'];
    $data['Direccion']          = $_REQUEST['txt_direccion'];
    $data['Estado']             = $_REQUEST['txt_estado'];
    $this-> model_Cliente -> EditCliente($data);
    $Mensaje = 'CLIENTE EDITADO CORRECTAMENTE!';
    $this -> Alerta($Mensaje);



  }

  function Delete()
  {
    //Lo agregamos todo en un arreglo
    $data = $_REQUEST['txt_Codigo'];
    $this-> model_Cliente -> DelCliente($data);
    $Mensaje = 'CLIENTE ELIMINADO CORRECTAMENTE!';
    $this -> Alerta($Mensaje);
    
  }

  function Buscar()
  {
    $data['Buscar']= $_REQUEST['txt_buscar'];
    $query =  $this-> model_Cliente -> BusCliente($data);

     if($query != false){
      require_once 'Views/header.php';
      //Index
      require_once 'Views/Cliente/index.php';
      //Footer
      require_once 'Views/footer.php';
      }

    else
    {
      $Mensaje = 'CLIENTE NO EXISTE!';
      $this -> Alerta($Mensaje);
  
    }


}


}
?>