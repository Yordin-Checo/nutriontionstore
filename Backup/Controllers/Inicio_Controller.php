<?php       

session_start();
require_once 'Models/Producto_Model.php';
Class Inicio_controller
{
   //Variables
   private $modelProducto;

   function __construct(){
       $this-> modelProducto = new Producto_Model;
   }

  function index()
  {
    $datos = $this-> modelProducto-> getProducto();
    $categoria = $this-> modelProducto-> getCategoria();
      //Header
      require_once 'Views/header.php';
      //Index
      require_once 'Views/Tienda/index.php';
      //Footer
      require_once 'Views/footer.php';
  }

   
   

}
?>