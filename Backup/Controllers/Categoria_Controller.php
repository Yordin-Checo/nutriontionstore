<?php
require_once 'Models/Categoria_Model.php';


class Categoria_Controller{
  
    //Variables
    private $modelCarrito;
   
    function __construct(){
        $this-> modelCategoria = new Categoria_Model;
    }

    function index(){
        $datos = $this-> modelCategoria-> getCategoria();
        require_once 'Views/header.php';
        require_once 'Views/Producto/Categoria.php';
        require_once 'Views/footer.php';
        }

        
  function Alerta($Mensaje1)
  {
     $datos = $this-> modelCategoria-> getCategoria(); 
      $Mensaje=$Mensaje1 ;
      require_once 'Views/header.php';
      require_once 'Views/Alerta.php';
      require_once 'Views/Producto/Categoria.php';
      require_once 'Views/footer.php';
  }
  
  function Create()
  {
    //Lo agregamos todo en un arreglo
    $data['Descripcion']       = $_REQUEST['txt_nombre'];
    $this-> modelCategoria -> addCategoria($data);
    $Mensaje = 'CATEGORÍA REGISTRADO CORRECTAMENTE!';
    $this -> Alerta($Mensaje);

  }

  
  function Editar()
  {
    //Lo agregamos todo en un arreglo
    $data['Codigo_Categoria']          = $_REQUEST['txt_Codigo'];
    $data['Descripcion']               = $_REQUEST['txt_nombre'];
   
    $this-> modelCategoria -> EditCategoria($data);
    $Mensaje = 'CATEGORÍA EDITADO CORRECTAMENTE!';
    $this -> Alerta($Mensaje);
  }

  function Delete()
  {
    //Lo agregamos todo en un arreglo
    $data = $_REQUEST['txt_Codigo'];
    $this-> modelCategoria -> DelCategoria($data);
    $Mensaje = 'CATEGORÍA ELIMINADO CORRECTAMENTE!';
    $this -> Alerta($Mensaje);
    
  }

  function Buscar()
  {
    $data['Buscar']= $_REQUEST['txt_buscar'];
    $datos =  $this-> modelCategoria -> BusCategoria($data);

     if($datos != false){
      require_once 'Views/header.php';
      require_once 'Views/Producto/Categoria.php';
      require_once 'Views/footer.php';
      }

    else
    {
      $Mensaje = 'CATEGORÍA NO EXISTE!';
      $this -> Alerta($Mensaje);
  
    }


}
    }

    ?>