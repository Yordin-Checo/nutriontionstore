<div class="container" styles="margin-top:80px">
    <div class="jumbotron">
       <h2 align="center">REGISTRO DE PRODUCTO</h2>
    </div>
   
    <hr />

<form class="form-inline " action="index.php?P=Buscar" method="Post" >
    <div class="form-group  col-md-5 ">
        <input type="text" class="form-control" name="txt_buscar" placeholder="Producto">
        &nbsp;
        <span class="input-group-btn" style="width:30%;">
            <input class="btn btn-dark text-white form-control" type="submit" value="Buscar" />
        </span>
    </div>

    <div class="form-group col-md-7 d-flex justify-content-end">
    <button type="button" class="btn btn-dark  form-control" data-toggle="modal" data-target="#exampleModal" data-whatever=""><i class="fas fa-cubes"></i>  Registrar nuevo</button>
    </div>
</form>
    

<hr />

<!-- Inicio-->
<div class="table-responsive">
    <table class="table table-bordered table-hover">
    <thead class="thead-dark">
                <tr>
                    <th scope="row">Código</th>
                    <th scope="row">Nombre</th>
                    <th scope="row">Descripción</th>
                    <th scope="row">Precio</th>
                    <th scope="row">Cantidad</th>                    
                    <th scope="row">Costo</th>
                    <th scope="row">Estado</th>  
                    <th scope="row">Categoría</th> 
                    <th scope="row">Acción</th>              

                </tr>
            </thead>
            <tbody>
                <?php foreach($datos as $data):?>
                <tr>
                    <td ><?php echo $data['IdProducto']?></td>
                    <td><?php echo $data['Nombre']?></td>
                    <td><?php echo $data['Descripcion']?></td>
                    <td><?php echo number_format($data['Precio'],2)?></td>                 
                    <td><?php echo number_format($data['Cantidad'],2)?></td>
                    <td><?php echo number_format($data['Costo'],2)?></td> 
                    <td><?php echo $data['Estado']?></td>                  
                    <td><?php echo $data['Categoria']?></td>
                    <td>
                    <a  class='text-success ml-1'  data-toggle='modal' data-target='#EditarP' onClick='selectProducto(this)'><i class="fa fa-marker fa-lg "></i></a>      
                    <a class="text-danger ml-1" data-toggle='modal' data-target='#EliminarP' onClick='selectDelete(this)'><i class="fa fa-trash-alt fa-lg "></i></a>
                    </td>
                </tr>
               <?php endforeach;?>
            </tbody>
        </table>
    </div>
</div>



<!-- Registrar-->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary" id="exampleModalLabel"><i class="fas fa-cubes"></i> NUEVO PRODUCTO</h5>
                <button type="button" class="close bg-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class ="container">
                  <div class="col-md-12">
        <div class="form-horizontal">
        
<form action="index.php?P=Create" method="post">
    <div class="form-row">
        <div class="form-group col-md-6">
             <label class="col-sm-2 control-label" for="txt_nombre">Nombre: </label>
                <div class="col-sm-11">
                    <input type="text" name="txt_nombreP" class="form-control" required>        
                </div>        
        </div>
        
        <div class="form-group col-md-6">
             <label class="col-sm-2 control-label" for="txt_categoria">Categoría: </label>
                <div class="col-sm-11">
                <select name="sel_categoria" required class="form-control input-sm" >
                <option value="0">-- Seleccione --</option>
                 <?php foreach($categoria as $row):?>
                      <option value="<?php echo $row['IdCategoria']?>"><?php echo $row['Descripcion']?></option>
                <?php endforeach; ?>									
                </select>       
                </div>
         </div>        
         
  </div>
    
    </div>
   
    <div class="form-row">
        <div class="form-group col-md-6">
             <label class="col-sm-2 control-label" for="txt_Nombre">Cantidad: </label>
                <div class="col-sm-11">
                    <input type="number" step='0.01' name="txt_cantidad" class="form-control" required>        
                </div>        
        </div>
        <div class="form-group col-md-6">
             <label class="col-sm-2 control-label" for="txt_Nombre">Costo: </label>
                <div class="col-sm-11">
                    <input type="number" required  step='0.01' name="txt_costo" class="form-control">        
                </div>        
        </div>
    </div>

    <div class="form-row">
    <div class="form-group col-md-6">
             <label class="col-sm-2 control-label" for="txt_apellido">Precio: </label>
                <div class="col-sm-11">
                    <input type="number" step='0.01' required name="txt_precio" class="form-control">        
                </div>        
        </div>
        <div class="form-group col-md-6">
             <label class="col-sm-2 control-label" for="txt_Nombre">Estado: </label>
                <div class="col-sm-11">
                    <input type="text" name="txt_estado" class="form-control" list="estado"  required>        
                </div>        
        </div>

        <datalist id="estado">
                <option value="Activo">A</option>
                <option value="Inactivo">I</option>
       </datalist>
 <div class="form-row">
    <div class="form-group col-md-12">
        <label class="col-sm-2 control-label" for="txt_apellido">Descripción: </label>
      <div class="col-sm-12">
          <textarea  class="form-control" maxlength="255" rows="5" name="txt_Descri" id="Descri" cols="87" required > </textarea>
      </div>
    </div>
</div>
  
</div>

   
</div>
</div> 
</div> 
        <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar </button>
                <button type="submit" class="btn btn-primary">Registrar</button>
            </div>
        </div>

    </Form>
    </div>
</div>

<!-- Editar-->
<div class="modal fade" id="EditarP" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary" id="exampleModalLabel"><i class="fas fa-cubes"></i> EDITAR PRODUCTO</h5>
                <button type="button" class="close bg-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class ="container">
                  <div class="col-md-12">
        <div class="form-horizontal">
        
<form action="index.php?P=Editar" method="post">
<input type="hidden" name="txt_Codigo" id="Codigo_Producto">
<div class="form-row">
        <div class="form-group col-md-6">
             <label class="col-sm-2 control-label" for="txt_nombre">Nombre: </label>
                <div class="col-sm-11">
                <input type="text" name="text_nombreP" required id="Nombre" class="form-control">        
                      
                </div>        
        </div>
        
      <div class="form-group col-md-6">
             <label class="col-sm-2 control-label" for="txt_categoria">Categoría: </label>
                <div class="col-sm-11">
                <select name="sel_categoria"  required class="form-control input-sm">
                <option value="0">-- Seleccione --</option>
                 <?php foreach($categoria as $row):?>
                      <option value="<?php echo $row['IdCategoria']?>"><?php echo $row['Descripcion']?></option>
                <?php endforeach; ?>									
                </select>       
                </div>
         </div>        
       
  </div>
    
    </div>
   
    <div class="form-row">
        <div class="form-group col-md-6">
             <label class="col-sm-2 control-label" for="txt_Nombre">Cantidad: </label>
                <div class="col-sm-11">
                    <input  step='0.01' name="txt_cantidad" id="Cantidad"  class="form-control" required>        
                </div>        
        </div>
        <div class="form-group col-md-6">
             <label class="col-sm-2 control-label" for="txt_Nombre">Costo: </label>
                <div class="col-sm-11">
             
                    <input    required  step='0.01' name="txt_costo" id="Costos" class="form-control">        
                </div>        
        </div>
    </div>

    <div class="form-row">
    <div class="form-group col-md-6">
             <label class="col-sm-2 control-label" for="txt_apellido">Precio: </label>
                <div class="col-sm-11">
                <input    step='0.01' name="txt_precio" id="Precio" class="form-control">        
                </div>        
        </div>
        <div class="form-group col-md-6">
             <label class="col-sm-2 control-label" for="txt_Nombre">Estado: </label>
                <div class="col-sm-11">
                    <input type="text" name="txt_estado" required id="Estado" class="form-control" list="estado">        
                </div>        
        </div>

        <datalist id="estado">
                <option value="Activo">A</option>
                <option value="Inactivo">I</option>
       </datalist>
    </div>
 <div class="form-row">
    <div class="form-group col-md-12">
        <label class="col-sm-2 control-label" for="txt_apellido">Descripción: </label>
        <div class="col-sm-11">
    <textarea  class="form-control" rows="5"  maxlength="255" name="txt_Descri" id="Descripcion" cols="87" > </textarea>
    </div>
    </div>
</div>
  
</div>

   
</div>
 
        <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar </button>
                <button type="submit" class="btn btn-info">Editar</button>
            </div>
        </div>

    </Form>
    </div>
</div>

</div>
<!-- Eliminar-->
<div class="modal fade" id="EliminarP" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title text-primary" id="exampleModalLabel"><i class="fas fa-restroom"></i> ELIMINAR PRODUCTO</h5>
                <button type="button" class="close bg-danger" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                 <div class ="container">
                  <div class="col-md-12">
        <div class="form-horizontal">


<form action="index.php?P=Delete" method="post">

<h3 class="text-danger" align="center">¿Seguro que de sea eliminar el producto?</h3>
<hr>
    <div class="form-row">
    <div class="form-group col-md-6">
             <label class="col-sm-6 control-label" for="txt_Codigo">Código Producto: </label>
                <div class="col-sm-10">
                    <input type="text" name="txt_Codigo" id="Id" class="form-control" readonly>        
                </div>        
        </div>
        <div class="form-group col-md-6">
             <label class="col-sm-2 control-label" for="txt_nombre">Nombre: </label>
                <div class="col-sm-10">
                    <input type="text" name="txt_nombre" id="Nom" class="form-control" readonly>        
                </div>        
        </div>
       

    </div>   
   </div>
</div>
</div> 
</div> 
        <div class="modal-footer">
                <button type="button" class="btn btn-info" data-dismiss="modal">Cerrar </button>
                <button type="submit" class="btn btn-danger">Eliminar</button>
            </div>
        </div>

    </Form>
    </div>
</div>


<script>
//Función para pasar los parametros a nuestra ventana modal
function selectProducto(boton) {

var celdas = $(boton).closest('tr').find('td');

var Codigo_Producto       = celdas.eq(0).text();
var Nombre                = celdas.eq(1).text();
var Descripcion           = celdas.eq(2).text();
var Precio                = celdas.eq(3).text();
var Cantidad              = celdas.eq(4).text();
var Costos                 = celdas.eq(5).text();
var Estado                = celdas.eq(6).text();
var Categoria             = celdas.eq(7).text();
 
$('#Costos').val(Costos); 
$('#Codigo_Producto').val(Codigo_Producto);
$('#Nombre').val(Nombre);
$('#Descripcion').val(Descripcion);
$('#Precio').val(Precio);
$('#Cantidad').val(Cantidad);

$('#Estado').val(Estado);
$('#Categoria').val(Categoria);

 
}


function selectDelete(boton)
{
var celdas = $(boton).closest('tr').find('td');

var Codigo_Producto     = celdas.eq(0).text();
var Descripcion         = celdas.eq(1).text();

$('#Id').val(Codigo_Producto);
$('#Nom').val(Descripcion);
}
</script>